depeszs shell utils
===================

Set of shell utils I wrote to help my work in unix shell.

## Requirements

To use these tools you generally need to use Bash shell and standard GNU
tools. If there are any specific requirements for specific tool, they will get
listed in its own section.

## News

You can find news about this project on [my blog](https://www.depesz.com/tag/shell_utils/).

## General utils

| Name | Description | Blog |
| ---- | ----------- | ---- |
| group_by | Groups values from input based on common value in some column. | [announce](https://www.depesz.com/2019/02/19/grouping-values-in-shell/) |
| pg_prettify | Tool to reformat SQL queries using https://paste.depesz.com/ service. | [announce](https://www.depesz.com/2022/09/21/prettify-sql-queries-from-command-line/) |
| ressh | Retries connecting to given ssh server, and then finally gives you shell there. | [announce](https://www.depesz.com/2021/01/06/reconnecting-to-ssh-servers/) |
| run_every | Runs given command in a loop, trying to run it as consistently every "x" (seconds, minutes, ...). | [announce](https://www.depesz.com/2022/11/22/run-a-command-every-x-seconds/) |
| vsleep | sleep with progress information. | [announce](https://www.depesz.com/2019/03/04/visual-sleep-in-shell-and-shell_utils-repo-information/) |

## TMUX related utils

These scripts are for [TMUX](http://tmux.github.io/) - if you're not familiar
with it - it's terminal multiplexer, kinda like old
[Screen](https://www.gnu.org/software/screen/) but newer and more powerful.

All of them are supposed to be run inside TMUX session.

| Name | Description | Blog |
| ---- | ----------- | ---- |
| tmux_send_to_many | Automates sending "keyboard" input into multiple panes at once. | [announce](https://www.depesz.com/2019/02/04/automation-for-doing-stuff-in-multiple-windows-in-tmux/) |
| tmux_wait_for_many | Waits for given string to show up in multiple panes at once. | [announce](https://www.depesz.com/2019/05/13/wait-for-programs-in-tmux-panes-to-end/) |
| tmux_watch_many | Shows last lines of all matching panes, every N seconds | [announce](https://www.depesz.com/2019/06/18/new-shell_util-to-watch-output-in-multiple-tmux-panes-at-once) |
